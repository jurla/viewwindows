Little tool to view screen- and window titles on classic AmigaOS
The source code also demonstrates how to handle CLI arguments.

native amiga compilation: gcc -noixemul vw.c -o vw

cross compilation on linux: m68k-amigaos-gcc -noixemul vw.c -o vw

native Amiga GCC 2.95.3: http://aminet.net/package/dev/gcc/ADE
linux -> Amiga GCC 2.95.3: http://aminet.net/package/dev/gcc/m68k-amigaos-gcc 
