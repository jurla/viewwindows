/*
    View windows transferred from asm to C.

    � 1996-2016 Juri Laihonen - freeware

    Abstract:   Shows frontmost screens title and
                window titles of that screen.

    30-Aug-2016 CLI arguments added
    31-Aug-2016 Screen finder

*/

#include <stdio.h>
#include <string.h>

#include "exec/types.h"
#include "intuition/intuitionbase.h"
#include "intuition/intuition.h"
#include "intuition/screens.h"

struct Library *IntuitionBase;
struct IntuitionBase *IBase;

#include "vw.h"

int main(int argc,char *argv[])
{
	struct Window *window;
	struct Screen *firstscreen;

	unsigned long lock = 0;                     /* ibLock, intuitionbasen lukko */
	unsigned int no = 1;                        /* window counter                */
	unsigned char arg_flags=0;
	int screen_arg_idx=0;                       /* named screen index */

	if(1<argc) arg_flags=vw_parse_args(argc,argv,&screen_arg_idx);

	if((VW_ARGS_VERSION==(arg_flags&VW_ARGS_VERSION))
	||(VW_ARGS_VERSION_ONLY==(arg_flags&VW_ARGS_VERSION_ONLY))) {
		printf( "ViewWindows v1.5c -- � 1996-2016 Juri Laihonen.\n");
		if(VW_ARGS_VERSION_ONLY==(VW_ARGS_VERSION_ONLY&arg_flags)) return 0;
	}

	if(VW_ARGS_HELP==(arg_flags&VW_ARGS_HELP)) {
		vw_help();
		return 0;
	}

	if(0==(IntuitionBase=(struct Library *)OpenLibrary( "intuition.library", 36L ))) {
		printf("Intuinion library did not open, cannot continue.\n");
		return 1;
	}

	IBase=(struct IntuitionBase *)IntuitionBase;
	lock = LockIBase(0);                /* Locking IBase is a must      */
	firstscreen = IBase->FirstScreen;   /* pointer to first screen      */
	UnlockIBase(lock);
	CloseLibrary(IntuitionBase);

// find screen
	if(VW_ARGS_FIND_SCREEN==(arg_flags&VW_ARGS_FIND_SCREEN)) {
		if(0==screen_arg_idx) {
			printf("error: screen name missing from argument list.\n\n");
			vw_help();
			return 1;
		}
		else if(screen_arg_idx>=argc) {
			printf("error: argument parser error, this is a text you never should see. :/\n");
			vw_help();
			return 1;
		}

		while(firstscreen) {
			if(0==strcmp(firstscreen->Title,argv[screen_arg_idx])) break;
			firstscreen=firstscreen->NextScreen;
		}

		if(NULL==firstscreen) {
			printf("error: screen \"%s\" not found.\n",argv[screen_arg_idx]);
			return 1;
		}
	}

	if(0==(arg_flags&VW_ARGS_NO_SCREEN)) printf("Screen name: %ls\n" , firstscreen->Title );

	window = firstscreen->FirstWindow;  /* Pointer to first window. */

	while(window)
	{
		// Don't show backdrop window if NOBACKDROP argument was given.
		if((VW_ARGS_HIDE_BACKDROP==(VW_ARGS_HIDE_BACKDROP&arg_flags))
		&&(WFLG_BACKDROP==(window->Flags & WFLG_BACKDROP))) {
			window = window->NextWindow;
			continue;
		}

		if(window->Title != 0) printf( "%ld. window\t\"%ls\"\n" , no , window->Title);
		else
		{
			printf( "%d. window\tNO NAME " , no );
			if((window->Flags & WFLG_BACKDROP) == WFLG_BACKDROP) {
				printf( "-> Backdrop" );
			}
			printf("\n");
		}
		++no;
		window = window->NextWindow;
	}

	if(1==no) printf("There are no windows on this screen.\n");

	return 0;
}

inline void ucase(char *str) {
	int i=0;
	while(i<strlen(str)) str[i++]&=0xdf;
}

inline unsigned int vw_parse_args(int argc,char *argv[],int *screen_arg_idx) {
	unsigned int flags=0;
	int i;

	for(i=1;i<argc;++i) {
		if(0==strcmp(argv[i],"?")) { flags|=VW_ARGS_HELP; break; }	// no need for other flags

		ucase(argv[i]);	// case insensitivity

		if(0==strcmp(argv[i],"VERSION")) { flags|=VW_ARGS_VERSION_ONLY; break; }	// no need for other flags, if this flag is up.
		if(0==strcmp(argv[i],"V")) { flags|=VW_ARGS_VERSION; continue; }
		if(0==strcmp(argv[i],"NOSCREEN")) { flags|=VW_ARGS_NO_SCREEN; continue; }
		if(0==strcmp(argv[i],"FINDSCREEN")) { 
			flags|=VW_ARGS_FIND_SCREEN;
			++i;
			if(i>=argc) break;	// error, screen name missing.
			*screen_arg_idx=i;
			continue;
		}
		if(0==strcmp(argv[i],"NOBACKDROP")) { flags|=VW_ARGS_HIDE_BACKDROP; continue; }
	}

	return flags;
}

void vw_help() {
	printf("Arguments:\n"
	"?                          This text.\n"
	"V                          Verbose.\n"
	"VERSION                    Version information only.\n"
	"NOSCREEN                   Exclude screen.\n"
	"NOBACKDROP                 Exclude backdrop windows.\n"
	"FINDSCREEN screen_name     View windows from \"screen_name\". (case sensitive)\n"
	);
}
