#ifndef _VW_H_
#define _VW_H_

/* Command line argument flags */
#define VW_ARGS_HELP           1   /* View help                           */
#define VW_ARGS_VERSION        2   /* Show version                        */
#define VW_ARGS_NO_SCREEN      4   /* Do not show screen title            */
#define VW_ARGS_FIND_SCREEN    8   /* Use screen with specific title      */
#define VW_ARGS_HIDE_BACKDROP 16   /* don't show backdrop windows.        */
#define VW_ARGS_VERSION_ONLY  32   /* version information only            */

inline unsigned int vw_parse_args(int arc,char *argv[],int *screen_arg_idx);
void vw_help();

#endif
